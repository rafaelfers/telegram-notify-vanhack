FROM python:alpine3.6

RUN apk add --no-cache libffi-dev openssl-dev build-base

ADD . /app

WORKDIR /app

RUN pip install -r requirements.txt
WORKDIR /app/app

ENTRYPOINT gunicorn --bind 0.0.0.0:8000 --timeout 160 wsgi:application